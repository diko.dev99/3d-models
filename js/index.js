/**
   @notes
   // !.Dont forget to import file
   1. three.min.js
   2. GLTFLoader.js
   3. OrbitControls.js this for controller 
*/ 

// Scene
const scene = new  THREE.Scene()

// Camera
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000)

// Renderer
const renderer = new THREE.WebGLRenderer()
renderer.setSize(window.innerWidth, window.innerHeight)
document.body.appendChild(renderer.domElement)

const geometry = new THREE.SphereGeometry(10, 10, 10) //setup size model
const material = new THREE.MeshNormalMaterial({ wireframe: true }) //setup shape 
const sphere = new THREE.Mesh(geometry, material) //combine geometry and material
scene.add(sphere)

const geometry2 = new THREE.SphereGeometry(10, 10, 10)
const material2 = new THREE.MeshNormalMaterial({ wireframe: true })
const sphere2 = new THREE.Mesh(geometry2, material2)
sphere2.position.x = 40 //position horizontal shape any x.y. or z
scene.add(sphere2)

const geometry3 = new THREE.SphereGeometry(10, 10, 10)
const material3 = new THREE.MeshNormalMaterial({ wireframe: true })
const sphere3 = new THREE.Mesh(geometry3, material3)
sphere3.position.x = -40 //position horizontal shape any x.y. or z
scene.add(sphere3) //position horizontal shape any x.y. or z

camera.position.z = 80 //camera position

const controls = new THREE.OrbitControls(camera, renderer.domElement) //set control camera position
controls.minDistance = 1 //position minimunm zoom
controls.maxDistance = 1000 //position maximal zoom


const animate = () => { //create function for animation and controls
   requestAnimationFrame(animate)
   
   // for rotation you can set rotation.x/y/z

   sphere.rotation.y += 0.03

   sphere2.rotation.y += 0.01

   sphere3.rotation.y += 0.02

   controls.update() //font forget to add this if you set controls models

   renderer.render(scene, camera)
}

animate()